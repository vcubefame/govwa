![Alt-Text](https://gitlab.com/whoissqr/govwa/badges/master/pipeline.svg)

#### this is for SAST demo to Grab!

- [x] Completed task
- [ ] Incomplete task
  - [ ] create issues from pipeline
  - [x] show badge for master branch health status
  - [x] run SAST for every single PR or commit
  - [x] run SAST report for every single PR or commit to print all outstanding issues
  - [x] run SAST gate for merge-request to master based on outstanding owasp issues


#### GoVWA
GoVWA (Go Vulnerable Web Application) is a vulnerable web application designed for pentester or programmers to learn the web application vulnerability that often occur in web applications. The vulnerabilities in GoVWA are OWASP Top 10 category. 
