import gitlab
import os

gl = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['ACCESS_TOKEN'])
project = gl.projects.get('25015464')

issue_details = {
'title': f'Sast gating failed...',
'description': f'Pipeline failed',
}
issue = project.issues.create(issue_details)
print(issue.iid)

editable_issue = project.issues.get(issue.iid, lazy=True)
editable_issue.title = 'sast gating edited'
editable_issue.save()
